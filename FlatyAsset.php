<?php
/*
 * 2014-12-14
 * @author Programmer Thailand <contact@programmerthailand.com>
 * http://shapebootstrap.net/item/flat-theme-free-responsive-multipurpose-site-template/
 */
namespace kongoon\theme\flaty;

use yii\web\AssetBundle;
class FlatyAsset extends AssetBundle
{
    public $sourcePath='@vendor/kongoon/yii2-theme-flaty/assets';
    public $baseUrl = '@web';
    
    public $css=[

        'css/bootstrap.min.css',

    ];
    
    public $js=[

    ];
    
    public $depends = [

    ];
    
    public function init() {
        parent::init();
    }
}
